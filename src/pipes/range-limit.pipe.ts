import { Injectable, PipeTransform } from "@nestjs/common";

@Injectable()
export class RangeLimitPipe implements PipeTransform<number, number> {

  constructor(
    private readonly min = Number.NEGATIVE_INFINITY,
    private readonly max = Number.POSITIVE_INFINITY,
    private readonly inc = 1,
  ) {}

  public transform(value: number) {
    if (value < this.min) value = this.min;
    if (value > this.max) value = this.max;
    value = Math.floor(value / this.inc) * this.inc;
    return value;
  }

}
