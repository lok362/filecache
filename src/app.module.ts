import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { isDev } from "./env";
import { resolve, join } from "path";
import { AuthModule } from "./modules/auth/auth.module";
import { FileStorageModule } from "./modules/file-storage/file-storage.module";

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      useFactory: () => {
        if (isDev()) {
          return {
            type: "sqlite",
            database: resolve(__dirname, "../development.sqlite"),
            entities: [join(__dirname, "entities/**/*.entity.{ts,js}")],
            synchronize: true,
          };
        } else {
          throw new Error("Production database configuration is not defined.");
        }
      },
    }),
    AuthModule,
    FileStorageModule,
  ],
})
export class AppModule {}
