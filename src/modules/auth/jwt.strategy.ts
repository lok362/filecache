import { Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy, ExtractJwt } from "passport-jwt";
import { AuthService } from "./auth.service";
import { getEnvValue } from "../../env";
import { JwtPayloadDto } from "../../dtos/jwt-payload.dto";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {

  constructor(private readonly authService: AuthService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: getEnvValue("PORT"),
    });
  }

  public async validate(payload: JwtPayloadDto) {
    return this.authService.validateJwtPayload(payload);
  }

}
