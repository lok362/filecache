import { Controller, Post, Body, UsePipes, ValidationPipe, Get, Res } from "@nestjs/common";
import { AuthService } from "./auth.service";
import { UserCredentialsDto } from "../../dtos/user.dto";
import { Authenticated } from "../../decorators/authenticated.decorators";
import { UserService } from "../user/user.service";
import { Response } from "express";
import { ApiOkResponse } from "@nestjs/swagger";
import { JwtDto } from "../../dtos/jwt.dto";
import { SuccessDto } from "../../dtos/success.dto";

@Controller("api/auth")
export class AuthController {

  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
  ) {}

  @ApiOkResponse({ type: JwtDto })
  @Post("register")
  @UsePipes(ValidationPipe)
  public async register(@Body() user: UserCredentialsDto, @Res() res: Response) {
    await this.userService.createAndSaveUser(user);
    const token = await this.authService.authenticateUser(user);
    res.statusCode = 200;
    res.send({ token });
  }

  @ApiOkResponse({ type: JwtDto })
  @Post()
  @UsePipes(ValidationPipe)
  public async signIn(@Body() user: UserCredentialsDto, @Res() res: Response) {
    const token = await this.authService.authenticateUser(user);
    res.statusCode = 200;
    res.send({ token });
  }

  @ApiOkResponse({ type: SuccessDto })
  @Get()
  @Authenticated()
  public async authCheck() {
    return { success: true };
  }

}
