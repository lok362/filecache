import { Injectable, UnauthorizedException, UsePipes, ValidationPipe } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { UserService } from "../user/user.service";
import { UserCredentialsDto } from "../../dtos/user.dto";
import { JwtPayloadDto } from "../../dtos/jwt-payload.dto";

@Injectable()
export class AuthService {

  constructor(
    private readonly jwtService: JwtService,
    private readonly userService: UserService,
  ) {}

  @UsePipes(ValidationPipe)
  public async authenticateUser(user: UserCredentialsDto) {
    const userEntity = await this.userService.findByUsername(user.username);
    if (!userEntity) throw new UnauthorizedException();
    const passwordIsCorrect = await this.userService.comparePassword(user.password, userEntity.password);
    if (!passwordIsCorrect) throw new UnauthorizedException();
    return this.jwtService.sign({ id: userEntity.id, username: userEntity.username });
  }

  @UsePipes(ValidationPipe)
  public async validateJwtPayload(payload: JwtPayloadDto) {
    const userEntity = await this.userService.findById(payload.id);
    if (!userEntity) throw new UnauthorizedException();
    return userEntity;
  }

}
