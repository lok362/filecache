import { Module, Global } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { getEnvValue } from "../../env";
import { UserModule } from "../user/user.module";
import { JwtStrategy } from "./jwt.strategy";
import { AuthController } from "./auth.controller";
import { AuthService } from "./auth.service";

@Global()
@Module({
  imports: [
    JwtModule.register({
      secretOrPrivateKey: getEnvValue("PORT"),
      signOptions: {
        expiresIn: "24h",
      },
    }),
    UserModule,
  ],
  providers: [
    AuthService,
    JwtStrategy,
  ],
  controllers: [AuthController],
})
export class AuthModule {}
