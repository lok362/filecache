import { Module } from "@nestjs/common";
import { isDev } from "../../env";
import { InjectRepository, TypeOrmModule } from "@nestjs/typeorm";
import { UserEntity } from "../../entities/user.entity";
import { Repository } from "typeorm";
import { UserService } from "./user.service";

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity])],
  providers: [UserService],
  exports: [UserService],
})
export class UserModule {

  constructor(
    @InjectRepository(UserEntity) private readonly userRepo: Repository<UserEntity>,
    private readonly userService: UserService,
  ) {
    if (isDev()) {
      // (Re)Populate development database with dev user
      this.userRepo.clear().then(() => {
        this.userService.createAndSaveUser({
          username: "user",
          password: "Passord123!",
        });
      });
    }
  }

}
