import { Injectable, BadRequestException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { UserEntity } from "../../entities/user.entity";
import { Repository } from "typeorm";
import * as bcrypt from "bcrypt";
import { UserCredentialsDto } from "../../dtos/user.dto";

@Injectable()
export class UserService {

  constructor(
    @InjectRepository(UserEntity) private readonly userRepo: Repository<UserEntity>,
  ) {}

  /** Creates a new user entity, saves it in the database, then returns it. */
  public async createAndSaveUser(credentials: UserCredentialsDto) {
    const { username, password } = credentials;

    const usernameAvailable = await this.usernameAvailable(username);
    if (!usernameAvailable) throw new BadRequestException(`Username "${username}" already taken.`);

    const passwordSecure = this.passwordIsSecure(password);
    if (!passwordSecure) throw new BadRequestException("Password is not secure.");

    const hashedPassword = await this.hashPassword(password);
    const userEntity = this.userRepo.create({
      username,
      password: hashedPassword,
    });
    await this.userRepo.save(userEntity);
    return userEntity;
  }

  /** Checks if a username is not already taken by another user. */
  public async usernameAvailable(username: string) {
    const user = await this.findByUsername(username);
    if (!user) return true;
    return false;
  }

  public async findByUsername(username: string) {
    return await this.userRepo.findOne({ where: { username } });
  }

  public async findById(id: string) {
    return await this.userRepo.findOne(id);
  }

  /** Checks whether a password contains atleast:
   *    - 1 lowercase alphabetical character
   *    - 1 uppercase alphabetical character
   *    - 1 numeric character
   *    - 1 special character (!@#$%^&*)
   *
   *    and is atleast 8 character long.
   */
  public passwordIsSecure(password: string): boolean {
    const regex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
    return regex.test(password);
  }

  /** Compares a plain-text password to a hashed password. */
  public comparePassword(password: string, hash: string): Promise<boolean> {
    return new Promise((res, rej) => {
      bcrypt.compare(password, hash, (err, same) => {
        if (err) return rej(err);
        res(same);
      });
    });
  }

  /** Hashes a password so it can be stored safely. */
  private hashPassword(password: string): Promise<string> {
    return new Promise((res, rej) => {
      bcrypt.hash(password, 10, (err, hash) => {
        if (err) return rej(err);
        res(hash);
      });
    });
  }

}
