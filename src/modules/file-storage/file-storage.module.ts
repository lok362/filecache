import { Module } from "@nestjs/common";
import { FileStorageService } from "./file-storage.service";
import { FileStorageController } from "./file-storage.controller";
import { TypeOrmModule } from "@nestjs/typeorm";
import { FileEntity } from "../../entities/file.entity";

@Module({
  imports: [
    TypeOrmModule.forFeature([FileEntity]),
  ],
  providers: [
    FileStorageService,
  ],
  controllers: [
    FileStorageController,
  ],
})
export class FileStorageModule {}
