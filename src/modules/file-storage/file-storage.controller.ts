import {
  Controller,
  Post,
  UseInterceptors,
  UploadedFile,
  Get,
  Query,
  ParseIntPipe,
  Param,
  Res,
  NotFoundException,
} from "@nestjs/common";
import { FileInterceptor } from "@nestjs/platform-express";
import { FileStorageService } from "./file-storage.service";
import { MulterFileObject } from "./interfaces";
import { RangeLimitPipe } from "../../pipes/range-limit.pipe";
import { Response } from "express";
import { ApiImplicitFile, ApiResponse, ApiOkResponse } from "@nestjs/swagger";
import { SuccessDto } from "../../dtos/success.dto";
import { FileInfoDto } from "../../dtos/file-info.dto";
import { FileListDto } from "../../dtos/file-list.dto";

@Controller("api/store")
export class FileStorageController {

  constructor(private readonly fileStorageService: FileStorageService) {}

  @ApiImplicitFile({ name: "file" })
  @ApiResponse({ status: 201, type: SuccessDto })
  @Post("upload")
  @UseInterceptors(FileInterceptor("file"))
  public async uploadFile(
    @UploadedFile() file: MulterFileObject,
  ) {
    await this.fileStorageService.storeFile(file);
    return { success: true };
  }

  @ApiOkResponse({ type: FileListDto })
  @Get()
  public async listFiles(
    @Query("page", ParseIntPipe, new RangeLimitPipe(1)) page: number,
    @Query("perPage", ParseIntPipe, new RangeLimitPipe(5, 50, 5)) perPage: number,
  ) {
    return this.fileStorageService.getFilesList(page, perPage);
  }

  @ApiOkResponse({ type: FileInfoDto })
  @Get(":id")
  public async getFileInfo(@Param("id") id: string) {
    const fileInfo = await this.fileStorageService.getFileInfo(id);
    if (!fileInfo) throw new NotFoundException();
    return fileInfo;
  }

  @ApiOkResponse({ description: "Responds with whatever the file is with appropriate headers." })
  @Get(":id/direct")
  public async getFile(
    @Param("id") id: string,
    @Res() res: Response,
  ) {
    const file = await this.fileStorageService.getFile(id);
    if (!file) throw new NotFoundException();
    res.setHeader("Content-Type", file.mimetype);
    res.setHeader("Content-Length", file.size);
    res.end(file.data, file.encoding);
  }

  @ApiOkResponse({ type: [FileInfoDto] })
  @Get("search/:query")
  public async searchFiles(
    @Param("query") searchQuery: string,
  ) {
    return this.fileStorageService.searchFiles(searchQuery);
  }

}
