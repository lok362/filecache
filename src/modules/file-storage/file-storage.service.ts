import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { FileEntity } from "../../entities/file.entity";
import { Repository } from "typeorm";
import { MulterFileObject } from "./interfaces";
import { FileInfoDto } from "../../dtos/file-info.dto";

@Injectable()
export class FileStorageService {

  constructor(
    @InjectRepository(FileEntity) private readonly fileRepo: Repository<FileEntity>,
  ) {}

  public async storeFile(file: MulterFileObject) {
    const fileEntity = this.fileRepo.create({
      data: file.buffer,
      size: file.size,
      mimetype: file.mimetype,
      encoding: file.encoding,
      name: file.originalname,
      created: new Date(),
    });
    await this.fileRepo.save(fileEntity);
  }

  public async getFilesList(page: number, perPage: number) {
    const filesCount = await this.fileRepo.count();
    let fileEntities: FileEntity[] = [];
    if (filesCount > 0) {
      fileEntities = await this.fileRepo.find({
        skip: (page - 1) * perPage,
        take: perPage,
      });
    }
    return {
      totalCount: filesCount,
      page,
      perPage,
      files: this.makeFileList(fileEntities),
    };
  }

  public async getFileInfo(id: string) {
    const fileEntity = await this.fileRepo.findOne(id);
    if (!fileEntity) return undefined;
    return this.makeFileInfo(fileEntity);
  }

  public async getFile(id: string) {
    return this.fileRepo.findOne(id);
  }

  public async searchFiles(searchQuery: string) {
    const searchObj = { q: "%" + searchQuery + "%" };
    const fileEntities = await this.fileRepo.createQueryBuilder("file")
      .where("file.name LIKE :q", searchObj)
      .orWhere("file.mimetype LIKE :q", searchObj)
      .orWhere("file.encoding LIKE :q", searchObj)
      .orWhere("file.created LIKE :q", searchObj)
      .getMany();
    return this.makeFileList(fileEntities);
  }

  private makeFileList(fileEntities: FileEntity[]) {
    const files: FileInfoDto[] = [];
    for (const file of fileEntities) {
      files.push(this.makeFileInfo(file));
    }
    return files;
  }

  private makeFileInfo(fileEntity: FileEntity): FileInfoDto {
    return {
      id: fileEntity.id,
      size: fileEntity.size,
      mimetype: fileEntity.mimetype,
      encoding: fileEntity.encoding,
      name: fileEntity.name,
      created: fileEntity.created.toISOString(),
    };
  }

}
