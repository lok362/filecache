import * as dotenv from "dotenv";
import { resolve } from "path";
import { existsSync, readFileSync } from "fs";

const getNodeEnv = () => process.env.NODE_ENV || "development";
export const isDev = () => getNodeEnv() === "development";

const envFilePath = resolve(__dirname, `../${getNodeEnv()}.env`);
if (!existsSync(envFilePath)) throw new Error(`Environment file ${envFilePath} does not exist.`);
const ENV = dotenv.parse(readFileSync(envFilePath));

export const getEnvValue = (key: string) => {
  const value = ENV[key];
  if (!value) throw new Error(`Requested environment value "${key}" is not defined in .env file.`);
  return value;
};
