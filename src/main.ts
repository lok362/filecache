import { NestFactory } from "@nestjs/core";
import { NestExpressApplication } from "@nestjs/platform-express";
import { AppModule } from "./app.module";
import * as compression from "compression";
import * as helmet from "helmet";
import * as RateLimit from "express-rate-limit";
import { SwaggerModule, DocumentBuilder } from "@nestjs/swagger";
import { resolve } from "path";
import { existsSync } from "fs";
import { Logger } from "@nestjs/common";
import { getEnvValue } from "./env";

(async () => {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  app.use(compression());
  app.use(helmet());
  app.use(new RateLimit({
    windowMs: 10 * 60 * 1000, // 10 minutes
    max: 100, // max 100 req per IP per windowMs
  }));

  const logger = new Logger("StaticAssets");
  const publicPath = resolve(__dirname, "../client/dist");
  if (existsSync(publicPath)) {
    app.useStaticAssets(publicPath, { prefix: "/" });
    logger.log(`Serving static assets from public folder ${publicPath}`);
  } else {
    logger.log(`Public folder ${publicPath} does not exist`);
  }

  const swaggerCfg = new DocumentBuilder()
    .setTitle("FileCache")
    .setDescription("FileCache API description.")
    .setVersion("0.5.0")
    .build();
  const swaggerDoc = SwaggerModule.createDocument(app, swaggerCfg);
  SwaggerModule.setup("api/docs", app, swaggerDoc);

  process.addListener("SIGTERM", () => {
    app.close().then(() => process.exit(0));
  });

  await app.listen(getEnvValue("PORT"));
// tslint:disable-next-line:no-console
})().catch(console.error);
