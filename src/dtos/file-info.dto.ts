import { IsString, IsInt, IsDateString } from "class-validator";
import { ApiModelProperty } from "@nestjs/swagger";

export class FileInfoDto {

  @ApiModelProperty()
  @IsString()
  public id: string;

  @ApiModelProperty()
  @IsInt()
  public size: number;

  @ApiModelProperty()
  @IsString()
  public mimetype: string;

  @ApiModelProperty()
  @IsString()
  public encoding: string;

  @ApiModelProperty()
  @IsString()
  public name: string;

  @ApiModelProperty()
  @IsDateString()
  public created: string;

}
