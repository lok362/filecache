import { IsString } from "class-validator";
import { ApiModelProperty } from "@nestjs/swagger";

export class JwtDto {

  @ApiModelProperty()
  @IsString()
  public token: string;

}
