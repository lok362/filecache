import { IsInt, IsArray, ValidateNested } from "class-validator";
import { FileInfoDto } from "./file-info.dto";
import { ApiModelProperty } from "@nestjs/swagger";

export class FileListDto {

  @ApiModelProperty()
  @IsInt()
  public totalCount: number;

  @ApiModelProperty()
  @IsInt()
  public page: number;

  @ApiModelProperty()
  @IsInt()
  public perPage: number;

  @ApiModelProperty({ type: [FileInfoDto] })
  @IsArray()
  @ValidateNested({ each: true })
  public files: FileInfoDto[];

}
