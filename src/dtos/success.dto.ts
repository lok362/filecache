import { IsBoolean } from "class-validator";
import { ApiModelProperty } from "@nestjs/swagger";

export class SuccessDto {

  @ApiModelProperty()
  @IsBoolean()
  public success: boolean;

}
