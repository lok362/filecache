import { IsString } from "class-validator";

export class JwtPayloadDto {

  @IsString()
  public id: string;

  @IsString()
  public username: string;

}
