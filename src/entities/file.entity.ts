import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity("file")
export class FileEntity {

  @PrimaryGeneratedColumn()
  public id: string;

  @Column()
  public data: Buffer;

  @Column()
  public size: number;

  @Column()
  public mimetype: string;

  @Column()
  public encoding: string;

  @Column()
  public name: string;

  @Column()
  public created: Date;

}
