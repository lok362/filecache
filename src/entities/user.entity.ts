import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";
import { UserCredentialsDto } from "../dtos/user.dto";

@Entity("user")
export class UserEntity implements UserCredentialsDto {

  @PrimaryGeneratedColumn()
  public id: string;

  @Column()
  public username: string;

  @Column()
  public password: string;

}
