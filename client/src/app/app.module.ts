import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";

import { RouterService } from "./shared/services/router.service";
import { FileService } from "./shared/services/file.service";

import { SharedModule } from "./shared/shared.module";
import { AuthModule } from "./modules/auth/auth.module";

import { AppComponent } from "./app.component";
import { LanderComponent } from "./components/lander/lander.component";
import { HeaderComponent } from "./components/header/header.component";
import { FileViewComponent } from "./components/file-view/file-view.component";
import { FileListComponent } from "./components/file-list/file-list.component";
import { FileBrowseComponent } from "./components/file-browse/file-browse.component";

library.add(fas);

const routes: Routes = [
  { path: "", component: LanderComponent },
  { path: "file/:id", component: FileViewComponent },
  { path: "browse", component: FileBrowseComponent },
  { path: "**", redirectTo: "" }
];

@NgModule({
  declarations: [
    AppComponent,
    LanderComponent,
    HeaderComponent,
    FileViewComponent,
    FileListComponent,
    FileBrowseComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FontAwesomeModule,
    SharedModule.forRoot(),
    AuthModule
  ],
  providers: [
    RouterService,
    FileService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(private readonly routerService: RouterService) {}

}
