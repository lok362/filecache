import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { FileService, FileInfoDto } from "src/app/shared/services/file.service";

@Component({
  selector: "app-file-browse",
  templateUrl: "./file-browse.component.html",
  styleUrls: ["./file-browse.component.scss"]
})
export class FileBrowseComponent implements OnInit {

  public page = 1;
  public pagesTotal: number;
  public files: FileInfoDto[];

  private perPage = 5;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly fileService: FileService,
    private readonly router: Router
  ) { }

  public ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (params.page) this.page = parseInt(params.page, 10);
      else this.router.navigate([], { queryParams: { page: 1 } });

      this.fileService.getFiles(this.page, this.perPage).subscribe(list => {
        this.page = list.page;
        this.pagesTotal = Math.floor(list.totalCount / this.perPage) + 1;
        this.perPage = list.perPage;
        this.files = list.files;
      });
    });
  }

  public select(id: string) {
    this.router.navigateByUrl(`/file/${id}`);
  }

}
