import { Component, OnInit, OnDestroy } from "@angular/core";
import { AuthService } from "../../shared/services/auth.service";
import { Subscription } from "rxjs";
import { Router } from "@angular/router";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit, OnDestroy {

  public signedIn: boolean;
  public username: string;

  private authSub: Subscription;

  constructor(
    private readonly auth: AuthService,
    private readonly router: Router
  ) {}

  public ngOnInit() {
    this.authSub = this.auth.subscribeToState(state => {
      this.signedIn = state.signedIn;
      if (state.signedIn) this.username = state.username;
      else this.username = undefined;
    });
  }

  public ngOnDestroy() {
    this.authSub.unsubscribe();
  }

  public onClickSignOut() {
    this.auth.signOut();
  }

  public onClickSignIn() {
    this.router.navigateByUrl("/auth/signin");
  }

  public onClickRegister() {
    this.router.navigateByUrl("/auth/register");
  }

}
