import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { FileService, FileInfoDto } from "../../shared/services/file.service";
import { ToastService, ToastLevel } from "../../shared/services/toast.service";
import { fadeInOut } from "src/shared-animations";

@Component({
  selector: "app-file-view",
  templateUrl: "./file-view.component.html",
  styleUrls: ["./file-view.component.scss"],
  animations: [fadeInOut]
})
export class FileViewComponent implements OnInit {

  public fileInfo: FileInfoDto;
  public file: any;
  public baseMimetype: string;
  public directLink: string;

  constructor(
    private readonly router: ActivatedRoute,
    private readonly fileService: FileService,
    private readonly toast: ToastService
  ) { }

  public ngOnInit() {
    this.router.params.subscribe(params => {
      const id = params.id;
      if (!id) return;
      this.loadFileInfo(id);
    });
  }

  private loadFileInfo(id: string) {
    this.fileService.getFileInfo(id).subscribe(
      fileInfo => {
        this.fileInfo = fileInfo;
        this.baseMimetype = fileInfo.mimetype.split("/")[0];
        this.directLink = `/api/store/${fileInfo.id}/direct`;
        this.loadFile(id);
      },
      err => {
        if (err.status === 404) {
          this.toast.make("File info not found.", ToastLevel.Error);
        } else {
          this.toast.make("Unexpected error. Please try again later.", ToastLevel.Error);
        }
      }
    );
  }

  private loadFile(id: string) {
    this.fileService.getFile(id).subscribe(
      file => {
        this.file = file;
      },
      err => {
        if (err.status === 404) {
          this.toast.make("Raw file not found.", ToastLevel.Error);
        } else {
          this.toast.make("Unexpected error. Please try again later.", ToastLevel.Error);
        }
      }
    );
  }

}
