import { Component, Input, Output, EventEmitter } from "@angular/core";
import { FileInfoDto } from "../../shared/services/file.service";

@Component({
  selector: "app-file-list",
  templateUrl: "./file-list.component.html",
  styleUrls: ["./file-list.component.scss"]
})
export class FileListComponent {

  @Input() public files: FileInfoDto[];

  @Output() public selectFile = new EventEmitter<string>();

  public baseMimetype = (mimetype: string) => mimetype.split("/")[0];
  public isImage = (mime: string) => this.baseMimetype(mime) === "image";
  public isAudio = (mime: string) => this.baseMimetype(mime) === "audio";
  public isVideo = (mime: string) => this.baseMimetype(mime) === "video";
  public isText = (mime: string) => this.baseMimetype(mime) === "text";
  public isUnknown = (mime: string) =>
    !this.isImage(mime) && !this.isVideo(mime) && !this.isText(mime) && !this.isAudio(mime)

  public fileNameWithoutExtension(file: FileInfoDto) {
    return file.name.match(/(^[^\.]+$|.+(?=\.))/)[0];
  }

}
