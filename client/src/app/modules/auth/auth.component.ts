import { Component, OnInit, OnDestroy } from "@angular/core";
import { AuthService } from "../../shared/services/auth.service";
import { ToastService, ToastLevel } from "../../shared/services/toast.service";

@Component({
  selector: "app-auth",
  templateUrl: "./auth.component.html",
  styleUrls: ["./auth.component.scss"]
})
export class AuthComponent implements OnInit, OnDestroy {
  private redirectTimer: number;

  constructor(
    private readonly auth: AuthService,
    private readonly toast: ToastService
  ) { }

  public ngOnInit() {
    if (this.auth.isSignedIn) {
      this.toast.make(`Already signed in as ${this.auth.username}.`, ToastLevel.Warn);
      this.redirectTimer = window.setTimeout(() => {
        this.auth.exit();
      }, 1000);
    }
  }

  public ngOnDestroy() {
    clearTimeout(this.redirectTimer);
  }

}
