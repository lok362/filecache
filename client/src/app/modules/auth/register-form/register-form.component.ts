import { Component, ViewChild, ElementRef, AfterViewInit } from "@angular/core";
import { AuthService } from "../../../shared/services/auth.service";
import { ToastService, ToastLevel } from "src/app/shared/services/toast.service";

@Component({
  selector: "app-register-form",
  templateUrl: "./register-form.component.html",
  styleUrls: ["register-form.component.scss"]
})
export class RegisterFormComponent implements AfterViewInit {

  @ViewChild("usernameEl", { static: false }) public usernameEl: ElementRef<HTMLInputElement>;
  @ViewChild("passwordEl", { static: false }) public passwordEl: ElementRef<HTMLInputElement>;
  @ViewChild("passwordRetypeEl", { static: false }) public passwordRetypeEl: ElementRef<HTMLInputElement>;

  public input = {
    username: "",
    password: "",
    passwordRetype: ""
  };

  public canSubmit = true;

  constructor(
    public readonly auth: AuthService,
    private readonly toast: ToastService
  ) { }

  public ngAfterViewInit() {
    this.usernameEl.nativeElement.focus();
  }

  public onSubmit() {
    if (!this.canSubmit) return;

    if (!this.input.username) {
      this.toast.make("You must supply a username to sign in.", ToastLevel.Error);
      this.usernameEl.nativeElement.focus();
    } else if (!this.input.password) {
      this.toast.make("You must supply a password to sign in.", ToastLevel.Error);
      this.passwordEl.nativeElement.focus();
    } else if (this.input.password !== this.input.passwordRetype) {
      this.toast.make("Passwords do not match.", ToastLevel.Error);
      this.input.passwordRetype = "";
      this.passwordEl.nativeElement.focus();
      setTimeout(() => this.passwordEl.nativeElement.select());
    } else {
      this.canSubmit = false;
      this.auth.register({ username: this.input.username, password: this.input.password },
        err => {
          if (err.status === 400) {
            this.toast.make(err.error.message, ToastLevel.Error);
          } else {
            this.toast.make("Unexpected error occured. Please try again later.", ToastLevel.Error);
          }
          this.canSubmit = true;
        },
        () => {
          this.toast.make("Successfully registered!");
          this.auth.exit();
        }
      );
    }
  }

  public onCancel() {
    this.auth.exit();
  }

}
