import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";

import { AuthService } from "../../shared/services/auth.service";

import { AuthComponent } from "./auth.component";
import { SignInFormComponent } from "./sign-in-form/sign-in-form.component";
import { RegisterFormComponent } from "./register-form/register-form.component";

const routes: Routes = [
  {
    path: "auth",
    component: AuthComponent,
    children: [
      { path: "signin", component: SignInFormComponent },
      { path: "register", component: RegisterFormComponent },
      { path: "**", redirectTo: "signin" }
    ]
  }
];

@NgModule({
  declarations: [
    AuthComponent,
    SignInFormComponent,
    RegisterFormComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    HttpClientModule
  ],
  exports: [
    RouterModule
  ]
})
export class AuthModule {}
