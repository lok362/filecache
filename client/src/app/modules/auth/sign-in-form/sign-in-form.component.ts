import { Component, ViewChild, ElementRef, AfterViewInit } from "@angular/core";
import { AuthService } from "../../../shared/services/auth.service";
import { ToastService, ToastLevel } from "src/app/shared/services/toast.service";

@Component({
  selector: "app-sign-in-form",
  templateUrl: "./sign-in-form.component.html"
})
export class SignInFormComponent implements AfterViewInit {

  @ViewChild("usernameEl", { static: false }) public usernameEl: ElementRef<HTMLInputElement>;
  @ViewChild("passwordEl", { static: false }) public passwordEl: ElementRef<HTMLInputElement>;

  public input = {
    username: "",
    password: ""
  };

  public canSubmit = true;

  constructor(
    public readonly auth: AuthService,
    private readonly toast: ToastService
  ) { }

  public ngAfterViewInit() {
    this.usernameEl.nativeElement.focus();
  }

  public onSubmit() {
    if (!this.canSubmit) return;

    if (!this.input.username) {
      this.toast.make("You must supply a username to sign in.", ToastLevel.Error);
      this.usernameEl.nativeElement.focus();
    } else if (!this.input.password) {
      this.toast.make("You must supply a password to sign in.", ToastLevel.Error);
      this.passwordEl.nativeElement.focus();
    } else {
      this.canSubmit = false;
      this.auth.signIn(this.input,
        err => {
          if (err.status === 401) {
            this.toast.make("Invalid credentials.", ToastLevel.Error);
            this.passwordEl.nativeElement.focus();
            setTimeout(() => this.passwordEl.nativeElement.select());
          } else {
            this.toast.make("Unexpected error occured. Please try again later.", ToastLevel.Error);
          }
          this.canSubmit = true;
        },
        () => {
          this.toast.make("Signed in successfully!");
          this.auth.exit();
        }
      );
    }
  }

  public onCancel() {
    this.auth.exit();
  }

}
