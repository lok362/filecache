import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";

import { ToastComponent } from "./components/toast/toast.component";
import { ToastService } from "./services/toast.service";
import { LoaderComponent } from "./components/loader/loader.component";
import { BytesPipe } from "./pipes/bytes.pipe";
import { PaginatorComponent } from "./components/paginator/paginator.component";
import { ImagePreviewComponent } from "./components/image-preview/image-preview.component";

library.add(fas);

@NgModule({
  declarations: [
    ToastComponent,
    LoaderComponent,
    BytesPipe,
    PaginatorComponent,
    ImagePreviewComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    BrowserAnimationsModule
  ],
  exports: [
    ToastComponent,
    LoaderComponent,
    BytesPipe,
    PaginatorComponent,
    ImagePreviewComponent
  ]
})
export class SharedModule {

  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        ToastService
      ]
    };
  }

}
