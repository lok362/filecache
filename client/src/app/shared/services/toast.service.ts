import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable({ providedIn: "root" })
export class ToastService {

  private readonly stack = new BehaviorSubject<Toast[]>([]);

  public subscribe(observer: (stack: Toast[]) => void) {
    return this.stack.subscribe(observer);
  }

  public make(message: string, level = ToastLevel.Info, time = 3000): Toast {
    const toast: Toast = {
      message,
      level,
      id: this.makeId(),
      close: () => {} // replaced below
    };

    this.stack.next([...this.stack.value, toast]);

    const removeToast = () => {
      const arr = [...this.stack.value];
      const i = arr.findIndex(t => t.id === toast.id);
      if (i !== -1) {
        arr.splice(i, 1);
        this.stack.next(arr);
      }
    };

    const handle = setTimeout(removeToast, time);

    toast.close = () => {
      clearTimeout(handle);
      removeToast();
    };

    return toast;
  }

  private makeId() {
    return Date.now().toString(36);
  }

}

export interface Toast {
  message: string;
  level: ToastLevel;
  id: string;
  close: () => void;
}

export enum ToastLevel {
  Info = "info",
  Warn = "warn",
  Error = "error"
}
