import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { RouterService } from "./router.service";
import { Router } from "@angular/router";
import { map } from "rxjs/operators";
import { OperatorFunction, BehaviorSubject } from "rxjs";
import * as decode from "jwt-decode";

@Injectable({ providedIn: "root" })
export class AuthService {

  public get token() {
    return this.pToken;
  }

  public get isSignedIn() {
    return this.state.value.signedIn;
  }

  public get username() {
    if (!this.state.value.signedIn) return "N/A";
    return this.state.value.username;
  }

  public get headers(): { Authorization: string } {
    return {
      Authorization: `Bearer ${this.pToken}`
    };
  }

  private previousUrl: string;
  private pToken: string;

  private state = new BehaviorSubject<AuthState>({ signedIn: false });

  private mapToken: OperatorFunction<{ token: string }, void> = map(res => {
    this.writeToken(res.token);
    this.pToken = res.token;
  });

  constructor(
    private readonly http: HttpClient,
    routerService: RouterService,
    private readonly router: Router
  ) {
    this.readToken();
    this.previousUrl = routerService.getPreviousUrl();
  }

  public subscribeToState(observer: (state: AuthState) => void) {
    return this.state.subscribe(observer);
  }

  public register(
    credentials: { username: string, password: string },
    error: (err: { status: number, error: { message: string } }) => void,
    complete: () => void
  ) {
    return this.http.post<{ token: string }>("/api/auth/register", credentials)
      .pipe<void>(this.mapToken)
      .subscribe(undefined, error, () => {
        this.state.next({
          signedIn: true,
          username: credentials.username
        });
        complete();
      });
  }

  public signIn(
    credentials: { username: string, password: string },
    error: (err: { status: number, error: { message: string } }) =>  void,
    complete: () => void
  ) {
    return this.http.post<{ token: string }>("/api/auth", credentials)
      .pipe<void>(this.mapToken)
      .subscribe(undefined, error, () => {
        this.state.next({
          signedIn: true,
          username: credentials.username
        });
        complete();
      });
  }

  public signOut() {
    this.clearToken();
    this.state.next({ signedIn: false });
  }

  public exit() {
    this.router.navigateByUrl(this.previousUrl);
  }

  private readToken() {
    this.pToken = window.localStorage.getItem("fc-token");
    if (this.pToken) {
      const payload = decode<{ id: string, username: string }>(this.pToken);
      this.state.next({
        signedIn: true,
        username: payload.username
      });
    }
  }

  private writeToken(token: string) {
    window.localStorage.setItem("fc-token", token);
  }

  private clearToken() {
    window.localStorage.removeItem("fc-token");
  }

}

export type AuthState = AuthStateNotSignedIn | AuthStateSignedIn;

export interface AuthStateBase {
  signedIn: boolean;
}

export interface AuthStateNotSignedIn extends AuthStateBase {
  signedIn: false;
}

export interface AuthStateSignedIn extends AuthStateBase {
  signedIn: true;
  username: string;
}
