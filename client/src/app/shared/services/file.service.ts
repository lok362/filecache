import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { AuthService } from "./auth.service";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class FileService {

  constructor(
    private readonly http: HttpClient,
    private readonly auth: AuthService
  ) { }

  public getFiles(page = 1, perPage = 5) {
    return this.http.get<FileListDto>(`/api/store?page=${page}&perPage=${perPage}`, {
      headers: this.auth.headers
    });
  }

  public getFileInfo(id: string) {
    return this.http.get<FileInfoDto>(`/api/store/${id}`, {
      headers: this.auth.headers
    });
  }

  public getFile(id: string) {
    return this.http.get(`/api/store/${id}/direct`, {
      headers: this.auth.headers,
      observe: "response",
      responseType: "blob"
    }).pipe(map(res => res.body));
  }

}

export interface FileInfoDto {
  id: string;
  size: number;
  mimetype: string;
  encoding: string;
  name: string;
  created: string;
}

export interface FileListDto {
  totalCount: number;
  page: number;
  perPage: number;
  files: FileInfoDto[];
}
