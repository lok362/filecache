import { Injectable } from "@angular/core";
import { Router, NavigationEnd } from "@angular/router";

@Injectable()
export class RouterService {

  private previousUrl: string;
  private currentUrl: string;

  constructor(router: Router) {
    this.currentUrl = router.url;
    router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        this.previousUrl = this.currentUrl;
        this.currentUrl = e.url;
      }
    });
  }

  public getPreviousUrl() {
    return this.previousUrl;
  }

}
