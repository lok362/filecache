import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "bytes" })
export class BytesPipe implements PipeTransform {

  public transform(value: number, args?: any): string {
    if (typeof value !== "number") throw new Error("BytesPipe value must be a number!");
    if (value > 1024000) return (value / 1024000).toFixed(2) + " MB";
    if (value > 1024) return (value / 1024).toFixed(2) + " KB";
    return value + " B";
  }

}
