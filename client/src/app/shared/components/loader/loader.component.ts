import { Component, Input } from "@angular/core";
import { fadeInOut } from "src/shared-animations";

@Component({
  selector: "app-loader",
  templateUrl: "./loader.component.html",
  styleUrls: ["./loader.component.scss"],
  animations: [fadeInOut]
})
export class LoaderComponent {

  @Input() public size: string;
  @Input() public message: string;

  constructor() { }

}
