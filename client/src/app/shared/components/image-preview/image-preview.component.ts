import { Component, Input, OnInit } from "@angular/core";
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";
import { fadeInOut } from "src/shared-animations";

@Component({
  selector: "app-image-preview",
  templateUrl: "./image-preview.component.html",
  styleUrls: ["./image-preview.component.scss"],
  animations: [fadeInOut]
})
export class ImagePreviewComponent implements OnInit {

  @Input() public blob: Blob;

  public src: SafeUrl;

  constructor(private readonly sanitizer: DomSanitizer) { }

  public ngOnInit() {
    this.src = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(this.blob));
  }

}
