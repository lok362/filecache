import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-paginator",
  templateUrl: "./paginator.component.html",
  styleUrls: ["./paginator.component.scss"]
})
export class PaginatorComponent implements OnInit {

  @Input() public total = 10;
  @Input() public current = 5;

  public get scope(): number[] {
    const arr: number[] = [];
    let n = this.current - 2;
    while (n < 1) n++;
    while (arr.length < 5) {
      arr.push(n++);
    }
    while (arr[arr.length - 1] > this.total) arr.pop();
    n = arr[0] - 1;
    while (arr.length < 5) {
      arr.unshift(n--);
    }
    while (arr[0] < 1) arr.shift();
    return arr;
  }

  public get pagesAfterScope() {
    return this.current + 2 < this.total;
  }

  public get pagesBeforeScope() {
    return this.current > 3;
  }

  constructor() { }

  public ngOnInit() {
  }

}
