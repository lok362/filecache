import { Component, OnInit, OnDestroy } from "@angular/core";
import { ToastService, Toast } from "../../services/toast.service";
import { Subscription } from "rxjs";
import { trigger, transition, animate, style, keyframes } from "@angular/animations";

@Component({
  selector: "app-toast",
  templateUrl: "./toast.component.html",
  styleUrls: ["./toast.component.scss"],
  animations: [
    trigger("anim", [
      transition("void => *", [
        style({ opacity: 0 }),
        animate(".3s ease-in", keyframes([
          style({ opacity: 0, transform: "translateY(50%)", offset: 0 }),
          style({ opacity: 1, transform: "translateY(0)", offset: 1.0 })
        ]))
      ]),
      transition("* => void", [
        style({ opacity: 1 }),
        animate(".3s ease-in", keyframes([
          style({ opacity: 1, transform: "translateY(0)", offset: 0 }),
          style({ opacity: 0, transform: "translateY(-10%)", offset: 1.0 })
        ]))
      ])
    ])
  ]
})
export class ToastComponent implements OnInit, OnDestroy {

  public stack: Toast[] = [];

  private sub: Subscription;

  constructor(private readonly service: ToastService) {}

  public ngOnInit() {
    this.sub = this.service.subscribe(stack => {
      this.stack = stack;
    });
  }

  public ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
