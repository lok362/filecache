import { trigger, transition, style, animate, keyframes } from "@angular/animations";

export const fadeInOut = trigger("fadeInOut", [
  transition("void => *", [
    style({ opacity: 0 }),
    animate(".3s ease-in", keyframes([
      style({ opacity: 0, offset: 0 }),
      style({ opacity: 1, offset: 1.0 })
    ]))
  ]),
  transition("* => void", [
    style({ opacity: 1 }),
    animate(".3s ease-in", keyframes([
      style({ opacity: 1, offset: 0 }),
      style({ opacity: 0, offset: 1.0 })
    ]))
  ])
]);
